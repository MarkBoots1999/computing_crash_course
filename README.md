Material used for the two-day computing crash course for the course *AP3082 Computational Physics*

This is a simplified version of the week-long [*Casimir programming course*](https://gitlab.kwant-project.org/pc/casimir_programming_course)

The material is organized in jupyter notebooks. You can use the computing server at https://hub.compphys.quantumtinkerer.tudelft.nl/ to run them. 
In order to copy the crash course materials there, [click this magic link](https://hub.compphys.quantumtinkerer.tudelft.nl/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgitlab.kwant-project.org%2Fcomputational_physics%2Fcomputing_crash_course&urlpath=lab%2Ftree%2Fcomputing_crash_course%2FREADME.md&branch=master)

The crash course is **based on self-study**, with the opportunity to ask questions to instructors during contact hours. The notebooks contain various exercises you can do to apply the material, and also contain example solutions. 

### Recommended material to be studied before the course

To be able to follow the course optimally, we require a basic knowledge of Python and the use of `numpy`, a library for numerical calculations in python. In addition, you will have to plot your data, this is most convenient from python, for example using the library `matplotlib`. Finally, we require you to store the progress of your project in a version controlled repository. For this you will make use of `git`.

We recommend the following distribution of topics for a two-day study:

*Day 1: Python*
- [Basic python programming](basic_python)
- The sections on `numpy` and `matplotlib` in [Scientific programming with numpy/matplotlib/scipy](numpy_scipy)

This material consists of jupyter notebooks with (mini-)exercises and solutions. If you have followed the Python version of the Computational Science course in the bachelor, you should already be familiar with these topics. Videos introducing these notebooks from a previous year are also available: [video basic Python](https://compphys.quantumtinkerer.tudelft.nl/crashcourse-basic_python/) and [video `numpy` and `matplotlib`](https://compphys.quantumtinkerer.tudelft.nl/crashcourse-scipystack/)

*Day 2: git*
- [Video tutorials on using `git` through a graphical user interface](https://compphys.quantumtinkerer.tudelft.nl/crashcourse-git/). 

### Optional material

When working on your project, you will also find the need to properly organize your code (this will be assessed, too). You can make use of the
following materials for self-study, and use the code written during the project as exercise material! Of course, you can also ask us for advice in class during the course.

- [Structuring python code](structuring_python_code)

In later projects, you can also benefit from the wide range of methods available in `scipy`:

- the section on `scipy` in [Scientific programming with numpy/matplotlib/scipy](numpy_scipy)

`git` is often also used directly from the command line. In order to use it you should understand some basics of the shell:

- [Basic shell](shell_basics)

A detailed description of how to use `git` from the command line can be found here

- [Using git from the command line](git_command_line)

You will recognize many of the steps we also saw in the graphical user interface.
